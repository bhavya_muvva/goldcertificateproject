package com.epam.rdmanagement.api;

import com.epam.rdmanagement.dto.AssociateDto;
import com.epam.rdmanagement.dto.BatchDto;
import com.epam.rdmanagement.exception.AssociateException;
import com.epam.rdmanagement.exception.BatchException;
import com.epam.rdmanagement.model.Associate;
import com.epam.rdmanagement.model.Batch;
import com.epam.rdmanagement.model.Practice;
import com.epam.rdmanagement.service.impl.AssociateServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@SpringBootTest
@AutoConfigureMockMvc
 class AssociateApiTest {

    @MockBean
    AssociateServiceImpl associateService;
    @Autowired
    MockMvc mockMvc;
    static Associate associate=new Associate();
    static AssociateDto associateDto=new AssociateDto();
    static Batch batch=new Batch();
    static BatchDto batchDto=new BatchDto();
    static Practice practice;
    @BeforeAll
    static void testData()
    {
        practice=Practice.JAVA;

        batch.setId(1);
        batch.setName("Rd java");
        batch.setPractice(practice);
        batch.setStartDate(new Date(2023,5,12));
        batch.setEndDate(new Date(2023,7,12));
        batch.setAssociates(Collections.emptyList());

        associate.setId(1);
        associate.setName("bhavya");
        associate.setEmail("bhavya@gmail.com");
        associate.setGender("F");
        associate.setCollege("Vignan");
        associate.setStatus("ACTIVE");
        associate.setBatch(batch);

        batchDto.setId(1);
        batchDto.setName("Rd java");
        batchDto.setPractice(practice);
        batchDto.setStartDate(new Date(2023,5,12));
        batchDto.setEndDate(new Date(2023,7,12));

        associateDto.setId(1);
        associateDto.setName("bhavya");
        associateDto.setEmail("bhavya@gmail.com");
        associateDto.setGender("F");
        associateDto.setCollege("Vignan");
        associateDto.setStatus("ACTIVE");



    }
    @Test
    void testCreate() throws Exception
    {
        Mockito.when(associateService.create(associateDto)).thenReturn(associateDto);
        mockMvc.perform(post("/rd/associates")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(associateDto)))
                .andExpect(status().isCreated());

    }

    @Test
    void testUpdate() throws Exception
    {
        Mockito.when(associateService.update(associateDto)).thenReturn(associateDto);
        mockMvc.perform(put("/rd/associates")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(associateDto)))
                .andExpect(status().isOk());
    }

    @Test
    void testGetByGender() throws Exception
    {
        Mockito.when(associateService.getByGender("F")).thenReturn(Collections.singletonList(associateDto));
        mockMvc.perform(get("/rd/associates/F")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    @Test
    void testGetByGenderBatchExceptionCase() throws Exception
    {
        Mockito.when(associateService.getByGender("F")).thenThrow(new BatchException("exception", HttpStatus.NOT_FOUND));
        mockMvc.perform(get("/rd/associates/F")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
    @Test
    void testGetByGenderAssociateExceptionCase() throws Exception
    {
        Mockito.when(associateService.getByGender("F")).thenThrow(new AssociateException("exception", HttpStatus.NOT_FOUND));
        mockMvc.perform(get("/rd/associates/F")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testGetByGenderRuntimeExceptionCase() throws Exception
    {
        Mockito.when(associateService.getByGender("F")).thenThrow(RuntimeException.class);
        mockMvc.perform(get("/rd/associates/F")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }
    @Test
    void testDelete() throws Exception
    {
        Mockito.doNothing().when(associateService).delete(1);
        mockMvc.perform(delete("/rd/associates/1"))
                .andExpect(status().isNoContent());
    }



}
