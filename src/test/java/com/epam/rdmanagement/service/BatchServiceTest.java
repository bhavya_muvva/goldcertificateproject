package com.epam.rdmanagement.service;

import com.epam.rdmanagement.dto.BatchDto;
import com.epam.rdmanagement.exception.BatchException;
import com.epam.rdmanagement.model.Batch;
import com.epam.rdmanagement.model.Practice;
import com.epam.rdmanagement.repository.BatchRepository;
import com.epam.rdmanagement.service.impl.BatchServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)

 class BatchServiceTest {

    @Mock
    BatchRepository batchRepository;
    @InjectMocks
    BatchServiceImpl batchService;
    static BatchDto batchDto=new BatchDto();
    static Batch batch=new Batch();
    static  Practice practice;
    @BeforeAll
    static void testData()
    {
        practice= Practice.JAVA;

        batch.setId(1);
        batch.setName("Rd java");
        batch.setPractice(practice);
        batch.setStartDate(new Date(2023,5,12));
        batch.setEndDate(new Date(2023,7,12));
        batch.setAssociates(Collections.emptyList());


        batchDto.setId(1);
        batchDto.setName("Rd java");
        batchDto.setPractice(practice);
        batchDto.setStartDate(new Date(2023,5,12));
        batchDto.setEndDate(new Date(2023,7,12));


    }
    @Test
    void testCreate()
    {

        Mockito.when(batchRepository.save(any(Batch.class))).thenReturn(batch);
        assertNotNull(batchService.create(batchDto));



    }
    @Test
    void testGetBatchById()
    {
        Mockito.when(batchRepository.findBatchById(1)).thenReturn(Optional.ofNullable(batch));
        assertNotNull(batchService.getBatchById(1));
        Mockito.verify(batchRepository).findBatchById(1);
    }
    @Test
    void testGetBatchByIdExceptionCase()
    {
        Mockito.when(batchRepository.findBatchById(any(Integer.class))).thenReturn(Optional.empty());
        assertThrows(BatchException.class,()->batchService.getBatchById(1));
        Mockito.verify(batchRepository).findBatchById(1);
    }

    @Test
    void testIsBatchExists()
    {
        Mockito.when(batchRepository.existsById(1)).thenReturn(true);
        assertTrue(batchService.isBatchExists(1));
        Mockito.verify(batchRepository).existsById(1);

    }
}
