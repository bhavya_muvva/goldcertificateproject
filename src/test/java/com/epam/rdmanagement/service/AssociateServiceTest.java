package com.epam.rdmanagement.service;

import com.epam.rdmanagement.dto.AssociateDto;
import com.epam.rdmanagement.dto.BatchDto;
import com.epam.rdmanagement.exception.AssociateException;
import com.epam.rdmanagement.exception.BatchException;
import com.epam.rdmanagement.model.Associate;
import com.epam.rdmanagement.model.Batch;
import com.epam.rdmanagement.model.Practice;
import com.epam.rdmanagement.repository.AssociateRepository;
import com.epam.rdmanagement.service.impl.AssociateServiceImpl;
import com.epam.rdmanagement.service.impl.BatchServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class AssociateServiceTest {

    @Mock
    AssociateRepository associateRepository;
    @Mock
    BatchServiceImpl batchService;

    @InjectMocks
    AssociateServiceImpl associateService;
    static Associate associate=new Associate();
    static AssociateDto associateDto=new AssociateDto();
    static Batch batch=new Batch();
    static BatchDto batchDto=new BatchDto();
    static Practice practice;
    @BeforeAll
    static void testData()
    {
        practice=Practice.JAVA;
        
        batch.setId(1);
        batch.setName("Rd java");
        batch.setPractice(practice);
        batch.setStartDate(new Date(2023,5,12));
        batch.setEndDate(new Date(2023,7,12));
        batch.setAssociates(Collections.emptyList());

        associate.setId(1);
        associate.setName("bhavya");
        associate.setEmail("bhavya@gmail.com");
        associate.setGender("F");
        associate.setCollege("Vignan");
        associate.setStatus("ACTIVE");
        associate.setBatch(batch);
        associate.getStatus();
        batch.getAssociates();
        practice.getPractice();

       batchDto.setId(1);
       batchDto.setName("Rd java");
       batchDto.setPractice(practice);
       batchDto.setStartDate(new Date(2023,5,12));
       batchDto.setEndDate(new Date(2023,7,12));

        associateDto.setId(1);
        associateDto.setName("bhavya");
        associateDto.setEmail("bhavya@gmail.com");
        associateDto.setGender("F");
        associateDto.setCollege("Vignan");
        associateDto.setStatus("ACTIVE");
        associateDto.setBatch(batchDto);

        

    }
    @Test
    void testCreate()
    {
        Mockito.when(associateRepository.save(any(Associate.class))).thenReturn(associate);
        Mockito.when(batchService.create(batchDto)).thenReturn(batchDto);
        assertNotNull(associateService.create(associateDto));

    }
    @Test
    void testUpdate()
    {
        Mockito.when(batchService.isBatchExists(1)).thenReturn(true);
        Mockito.when(batchService.create(any(BatchDto.class))).thenReturn(batchDto);
        Mockito.when(associateRepository.save(any(Associate.class))).thenReturn(associate);
        assertNotNull(associateService.update(associateDto));

    }
    @Test
    void testUpdateExceptionCase()
    {
        Mockito.when(batchService.isBatchExists(any(Integer.class))).thenReturn(false);
        assertThrows( AssociateException.class,()->associateService.update(associateDto));
        Mockito.verify(batchService).isBatchExists(1);

    }
    @Test
    void testDelete()
    {
        Mockito.doNothing().when(associateRepository).deleteById(1);
        associateService.delete(1);
        Mockito.verify(associateRepository).deleteById(1);

    }
    @Test
    void testGetByGender()  {
        Mockito.when(associateRepository.findAssociateByGender("F")).thenReturn(Collections.singletonList(associate));
        assertNotNull(associateService.getByGender("F"));
        Mockito.verify(associateRepository).findAssociateByGender("F");
    }




}
