package com.epam.rdmanagement.helper;

public enum LoggingStrings {
    ENTERING_SERVICE_METHOD("Entered {} method of Service class : {} with data {} "),
    EXITING_SERVICE_METHOD("Exiting {} method of Service class : {} with data {}"),
    ENTERING_CONTROLLER_METHOD("Entered {} method of Controller : {} with data {}"),
    EXITING_CONTROLLER_METHOD("Exiting {} method of Controller : {} with data {}"),
    EXCEPTION_MESSAGE("Exception risen in {} method of {} class ,Exiting the method");
    private final String value;

    LoggingStrings(String value) {
        this.value=value;
    }
    public String getValue()
    {
        return value;
    }
}
