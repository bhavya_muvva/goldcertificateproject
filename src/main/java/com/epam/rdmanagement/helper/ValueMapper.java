package com.epam.rdmanagement.helper;

import com.epam.rdmanagement.dto.AssociateDto;
import com.epam.rdmanagement.dto.BatchDto;
import com.epam.rdmanagement.model.Associate;
import com.epam.rdmanagement.model.Batch;
import org.springframework.stereotype.Component;

@Component
public class ValueMapper {
    private ValueMapper(){}

    public static BatchDto convertBatchToDto(Batch batch) {
        BatchDto batchDto = new BatchDto();
        batchDto.setId(batch.getId());
        batchDto.setName(batch.getName());
        batchDto.setPractice(batch.getPractice());
        batchDto.setStartDate(batch.getStartDate());
        batchDto.setEndDate(batch.getEndDate());

        return batchDto;
    }
    public  static Batch convertDtoToBatch(BatchDto batchDto) {
        Batch batch = new Batch();
        batch.setId(batchDto.getId());
        batch.setName(batchDto.getName());
        batch.setPractice(batchDto.getPractice());
        batch.setStartDate(batchDto.getStartDate());
        batch.setEndDate(batchDto.getEndDate());

        return batch;
    }

    public static AssociateDto convertAssociateToDto(Associate associate) {
        AssociateDto associateDto = new AssociateDto();
        associateDto.setId(associate.getId());
        associateDto.setName(associate.getName());
        associateDto.setGender(associate.getGender());
        associateDto.setEmail(associate.getEmail());
        associateDto.setCollege(associate.getCollege());
        associateDto.setBatch(convertBatchToDto(associate.getBatch()));
        return associateDto;
    }

    public  static Associate convertDtoToAssociate(AssociateDto associateDto) {
        Associate associate = new Associate();
       associate.setId(associateDto.getId());
        associate.setName(associateDto.getName());
        associate.setGender(associateDto.getGender());
        associate.setEmail(associateDto.getEmail());
        associate.setCollege(associateDto.getCollege());
        return associate;
    }
}
