package com.epam.rdmanagement.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Associate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)

    private String name;
    @Column(nullable = false)

    private String email;
    @Column(nullable = false)

    private String gender;
    @Column(nullable = false)

    private  String college;


    private  String status;

    @ManyToOne
    private Batch batch;


}
