package com.epam.rdmanagement.model;

public enum Practice {
    JAVA("Java"),
    TESTING("Testing"),
    DEP("Dep"),
    DOTNET("DotNet");
    public final String practiceString;

    Practice(String practice) {
        this.practiceString =practice;
    }

    public String getPractice() {
        return practiceString;
    }
}
