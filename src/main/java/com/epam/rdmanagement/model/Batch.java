package com.epam.rdmanagement.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static jakarta.persistence.EnumType.ORDINAL;
@Getter
@Entity
@Setter
@RequiredArgsConstructor
public class Batch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String name;
    @Enumerated(ORDINAL)
    private Practice practice;

    @Column(nullable = false)
    private Date startDate;

    @Column(nullable = false)
    private  Date endDate;
    @OneToMany(mappedBy = "batch",cascade = CascadeType.ALL)
    List<Associate> associates=new ArrayList<>();

}
