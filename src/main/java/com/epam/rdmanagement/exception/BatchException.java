package com.epam.rdmanagement.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
@Getter
public class BatchException extends RuntimeException{
    private final HttpStatus httpStatus;
    public BatchException(String message,HttpStatus httpStatus)
    {
        super(message);
        this.httpStatus=httpStatus;
    }
}
