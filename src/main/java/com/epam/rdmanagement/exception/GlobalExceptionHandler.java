package com.epam.rdmanagement.exception;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    ExceptionResponse exceptionResponse;
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ExceptionResponse> handleArgumentNotValidException(MethodArgumentNotValidException exception, WebRequest request)
    {
        log.error(ExceptionUtils.getStackTrace(exception));
        StringBuilder error=new StringBuilder();
        exception.getAllErrors().forEach(err->error.append(err.getDefaultMessage()+"|"));
        exceptionResponse=new ExceptionResponse(new Date().toString(),exception.getMessage(), HttpStatus.BAD_REQUEST.name(), request.getDescription(false));
        return new ResponseEntity<>(exceptionResponse,HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(BatchException.class)
    public ResponseEntity<ExceptionResponse> handleBatchException(BatchException exception,WebRequest request)
    {
        log.error(ExceptionUtils.getStackTrace(exception));
        exceptionResponse=new ExceptionResponse(new Date().toString(),exception.getMessage(),exception.getHttpStatus().name(),request.getDescription(false));
        return new ResponseEntity<>(exceptionResponse,exception.getHttpStatus());

    }
    @ExceptionHandler(AssociateException.class)
    public ResponseEntity<ExceptionResponse> handleAssociateException(AssociateException exception,WebRequest request)
    {
        log.error(ExceptionUtils.getStackTrace(exception));
        exceptionResponse=new ExceptionResponse(new Date().toString(),exception.getMessage(),exception.getHttpStatus().name(),request.getDescription(false));
        return new ResponseEntity<>(exceptionResponse,exception.getHttpStatus());

    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ExceptionResponse> handleException(RuntimeException exception,WebRequest request)
    {
        log.error(ExceptionUtils.getStackTrace(exception));
        exceptionResponse=new ExceptionResponse(new Date().toString(),exception.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR.name(), request.getDescription(false));
        return new ResponseEntity<>(exceptionResponse,HttpStatus.INTERNAL_SERVER_ERROR);


    }


}

