package com.epam.rdmanagement.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
@Getter
@AllArgsConstructor
public class ExceptionResponse {
    String errorTime;
    String message;
    String httpStatus;
    String requestDescription;
}
