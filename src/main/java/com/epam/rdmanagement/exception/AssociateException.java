package com.epam.rdmanagement.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter

public class AssociateException extends RuntimeException{
    private final HttpStatus httpStatus;
    public AssociateException(String message,HttpStatus httpStatus)
    {
        super(message);
        this.httpStatus=httpStatus;
    }
}
