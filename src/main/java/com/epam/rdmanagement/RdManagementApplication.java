package com.epam.rdmanagement;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition
public class RdManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(RdManagementApplication.class, args);
    }

}
