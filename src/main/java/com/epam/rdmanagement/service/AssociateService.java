package com.epam.rdmanagement.service;

import com.epam.rdmanagement.dto.AssociateDto;

import java.util.List;

public interface AssociateService {
     AssociateDto create(AssociateDto associateDto);
     AssociateDto update(AssociateDto associateDto);
    List<AssociateDto> getByGender(String gender);
    void delete(int id);

}
