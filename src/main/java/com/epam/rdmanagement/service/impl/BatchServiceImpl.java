package com.epam.rdmanagement.service.impl;

import com.epam.rdmanagement.dto.BatchDto;
import com.epam.rdmanagement.exception.BatchException;
import com.epam.rdmanagement.helper.LoggingStrings;
import com.epam.rdmanagement.helper.ValueMapper;
import com.epam.rdmanagement.model.Batch;
import com.epam.rdmanagement.repository.BatchRepository;
import com.epam.rdmanagement.service.BatchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
@Slf4j
@Service
@RequiredArgsConstructor
public class BatchServiceImpl implements BatchService {
    public static final String GET_BY_ID = "getById";
    public static final String CREATE_METHOD = "create";
    public static final String IS_BATCH_EXISTS = "isBatchExists";
    private final BatchRepository batchRepository;

    @Override
    public BatchDto getBatchById(int id)
    {
        log.info(LoggingStrings.ENTERING_SERVICE_METHOD.getValue(), GET_BY_ID,this.getClass().getSimpleName(),id);
        Batch batch=batchRepository.findBatchById(id)
                .orElseThrow(()->new BatchException("Batch doesn't found with given id", HttpStatus.NOT_FOUND));
        BatchDto batchDto=ValueMapper.convertBatchToDto(batch);
        log.info(LoggingStrings.EXITING_SERVICE_METHOD.getValue(), GET_BY_ID,this.getClass().getSimpleName(),batchDto);
        return batchDto;

    }
    @Override
    public BatchDto create(BatchDto batchDto)
    {
        log.info(LoggingStrings.ENTERING_SERVICE_METHOD.getValue(), CREATE_METHOD,this.getClass().getSimpleName(),batchDto);
        Batch batch=ValueMapper.convertDtoToBatch(batchDto);
        batchDto=ValueMapper.convertBatchToDto(batchRepository.save(batch));
        log.info(LoggingStrings.EXITING_SERVICE_METHOD.getValue(), CREATE_METHOD,this.getClass().getSimpleName(),batchDto);
        return batchDto;
    }
    @Override
    public  boolean isBatchExists(int id)
    {
        log.info(LoggingStrings.ENTERING_SERVICE_METHOD.getValue(), IS_BATCH_EXISTS,this.getClass().getSimpleName(),id);
        boolean result= batchRepository.existsById(id);
        log.info(LoggingStrings.EXITING_SERVICE_METHOD.getValue(), IS_BATCH_EXISTS,this.getClass().getSimpleName(),result);
        return  result;


    }

}
