package com.epam.rdmanagement.service.impl;

import com.epam.rdmanagement.dto.AssociateDto;
import com.epam.rdmanagement.exception.AssociateException;
import com.epam.rdmanagement.exception.BatchException;
import com.epam.rdmanagement.helper.LoggingStrings;
import com.epam.rdmanagement.helper.ValueMapper;
import com.epam.rdmanagement.model.Associate;
import com.epam.rdmanagement.repository.AssociateRepository;
import com.epam.rdmanagement.service.AssociateService;
import com.epam.rdmanagement.service.BatchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class AssociateServiceImpl implements AssociateService {
    public static final String CREATE_METHOD = "create";
    public static final String UPDATE_METHOD = "update";
    public static final String DELETE_METHOD = "delete";
    public static final String GET_BY_GENDER = "getByGender";
    private final AssociateRepository associateRepository;
    private final BatchService batchService;
    @Override
    public AssociateDto create(AssociateDto associateDto)
    {
        log.info(LoggingStrings.ENTERING_SERVICE_METHOD.getValue(), CREATE_METHOD,this.getClass().getSimpleName(),associateDto);
        Associate associate= ValueMapper.convertDtoToAssociate(associateDto);
        associateDto.setBatch(batchService.create(associateDto.getBatch()));
        associate.setBatch(ValueMapper.convertDtoToBatch(associateDto.getBatch()));
        associate=associateRepository.save(associate);
        associateDto=ValueMapper.convertAssociateToDto(associate);
        log.info(LoggingStrings.EXITING_SERVICE_METHOD.getValue(), CREATE_METHOD,this.getClass().getSimpleName(),associateDto);
        return associateDto;

    }
    @Override

    public AssociateDto update(AssociateDto associateDto)
    {
        log.info(LoggingStrings.ENTERING_SERVICE_METHOD.getValue(), UPDATE_METHOD,this.getClass().getSimpleName(),associateDto);
        if(batchService.isBatchExists(associateDto.getBatch().getId())) {
            Associate associate= ValueMapper.convertDtoToAssociate(associateDto);
            associateDto.setBatch(batchService.create(associateDto.getBatch()));
            associate.setBatch(ValueMapper.convertDtoToBatch(associateDto.getBatch()));
            associate=associateRepository.save(associate);
            associateDto=ValueMapper.convertAssociateToDto(associate);
        }
        else {
            log.info(LoggingStrings.EXCEPTION_MESSAGE.getValue(), UPDATE_METHOD,this.getClass().getSimpleName());
            throw new AssociateException(" Invalid Batch Id for the associate", HttpStatus.NOT_FOUND);
        }
        log.info(LoggingStrings.EXITING_SERVICE_METHOD.getValue(), UPDATE_METHOD,this.getClass().getSimpleName(),associateDto);
        return associateDto;

    }
    @Override

    public List<AssociateDto> getByGender(String gender)
    {
        log.info(LoggingStrings.ENTERING_SERVICE_METHOD.getValue(), GET_BY_GENDER,this.getClass().getSimpleName(),gender);

        List<Associate> associates=associateRepository.findAssociateByGender(gender);
        List<AssociateDto> associateDtos=new ArrayList<>();
        associates.forEach(associate -> associateDtos.add(ValueMapper.convertAssociateToDto(associate)));
        log.info(LoggingStrings.EXITING_SERVICE_METHOD.getValue(), GET_BY_GENDER,this.getClass().getSimpleName(),associateDtos);
        return associateDtos;
    }

    @Override

    public void delete(int id)
    {
        log.info(LoggingStrings.ENTERING_SERVICE_METHOD.getValue(), DELETE_METHOD,this.getClass().getSimpleName(),id);
        associateRepository.deleteById(id);
        log.info(LoggingStrings.EXITING_SERVICE_METHOD.getValue(),DELETE_METHOD,this.getClass().getSimpleName());

    }




}
