package com.epam.rdmanagement.service;

import com.epam.rdmanagement.dto.BatchDto;

public interface BatchService {
    BatchDto getBatchById(int id);
    BatchDto create(BatchDto batchDto);
    boolean isBatchExists(int id);
}
