package com.epam.rdmanagement.api;

import com.epam.rdmanagement.dto.AssociateDto;
import com.epam.rdmanagement.helper.LoggingStrings;
import com.epam.rdmanagement.service.AssociateService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rd/associates")
@RequiredArgsConstructor
@Slf4j
public class AssociateApi {

    public static final String CREATE_METHOD = "create";
    public static final String UPDATE_METHOD = "update";
    public static final String GET_GENDER = "getGender";
    public static final String DELETE_METHOD = "delete";
    private  final AssociateService associateService;
    @PostMapping
    public ResponseEntity<AssociateDto> create(@Valid @RequestBody  AssociateDto associateDto)
    {
        log.info(LoggingStrings.ENTERING_CONTROLLER_METHOD.getValue(), CREATE_METHOD,this.getClass().getSimpleName(),associateDto);
        associateDto=associateService.create(associateDto);
        ResponseEntity<AssociateDto> response=new ResponseEntity<>(associateDto, HttpStatus.CREATED);
        log.info(LoggingStrings.EXITING_CONTROLLER_METHOD.getValue(), CREATE_METHOD,this.getClass().getSimpleName(),associateDto);
        return  response;

    }
    @PutMapping
    public ResponseEntity<AssociateDto> update(@RequestBody @Valid AssociateDto associateDto)
    {
        log.info(LoggingStrings.ENTERING_CONTROLLER_METHOD.getValue(), UPDATE_METHOD,this.getClass().getSimpleName(),associateDto);
        associateDto=associateService.update(associateDto);
        ResponseEntity<AssociateDto> response=new ResponseEntity<>(associateDto, HttpStatus.OK);
        log.info(LoggingStrings.EXITING_CONTROLLER_METHOD.getValue(), UPDATE_METHOD,this.getClass().getSimpleName(),associateDto);
        return  response;

    }
    @GetMapping("/{gender}")
    public ResponseEntity<List<AssociateDto>> getByGender(@PathVariable @NotBlank String gender)
    {
        log.info(LoggingStrings.ENTERING_CONTROLLER_METHOD.getValue(), GET_GENDER,this.getClass().getSimpleName(),gender);
        List<AssociateDto> associateDtos=associateService.getByGender(gender);
        ResponseEntity<List<AssociateDto>> response=new ResponseEntity<>(associateDtos, HttpStatus.OK);
        log.info(LoggingStrings.EXITING_CONTROLLER_METHOD.getValue(), GET_GENDER,this.getClass().getSimpleName(),associateDtos);
        return response;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable @NotBlank int id)
    {
        log.info(LoggingStrings.ENTERING_CONTROLLER_METHOD.getValue(), DELETE_METHOD,this.getClass().getSimpleName(),id);
        associateService.delete(id);
        ResponseEntity<Void> response=new ResponseEntity<>( HttpStatus.NO_CONTENT);
        log.info(LoggingStrings.EXITING_CONTROLLER_METHOD.getValue(), DELETE_METHOD,this.getClass().getSimpleName());
        return  response;

    }

}
