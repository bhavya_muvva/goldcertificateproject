package com.epam.rdmanagement.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class AssociateDto {
    @NotBlank(message = "Please enter the Associate ID ")

    private int id;
    @NotBlank(message = "Please enter the Associate name ")


    private String name;
    @Email(message = "Please enter valid email ")

    private String email;
    @NotBlank(message = "Please enter the Associate gender ")


    private String gender;
    @NotBlank(message = "Please enter the Associate college ")


    private  String college;

    @NotBlank(message = "Please enter the Associate status ")

    private  String status;
    @NotBlank(message = "Please enter the Batch details which Associates belongs ")


    private BatchDto batch;
}
