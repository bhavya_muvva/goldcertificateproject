package com.epam.rdmanagement.dto;

import com.epam.rdmanagement.model.Practice;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
@RequiredArgsConstructor
public class BatchDto {
    @NotBlank(message = "Please enter the batch ID ")
    private int id;
    @NotBlank(message = "Please enter the batch name ")

    private String name;
    @NotBlank(message = "Please enter the practice ")

    private Practice practice;
    @NotBlank(message = "Please enter the start Date ")

    private Date startDate;
    @NotBlank(message = "Please enter the end Date ")


    private  Date endDate;

}
