package com.epam.rdmanagement.repository;

import com.epam.rdmanagement.model.Associate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AssociateRepository extends JpaRepository<Associate,Integer> {

    List<Associate> findAssociateByGender(String gender);
}
