package com.epam.rdmanagement.repository;

import com.epam.rdmanagement.model.Batch;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BatchRepository extends JpaRepository<Batch,Integer> {
    Optional<Batch> findBatchById(int id);
}
